CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY : all
all : program
program : romandigitconverter.cpp numberconversion.o
	$(CXX) $(CXXFLAGS) -o program romandigitconverter.cpp numberconversion.o

numberconversion.o : numberconversion.cpp numberconversion.h
	$(CXX) $(CXXFLAGS) -c numberconversion.cpp
	$(CXX) $(CXXFLAGS) -c numberconversion.h

.PHONY : clean
clean :
	$(RM) *.o
	$(RM) program

